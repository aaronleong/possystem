from django.urls import path

from . import views

app_name = 'location'
urlpatterns = [
    path('', views.location_list_view),
    path('<int:my_id>/', views.location_detail_view),
    path('parcel-location-new/', views.location_create_view),
    path('<int:my_id>/delete', views.location_delete_view),
    path('<int:my_id>/edit', views.location_update_view),
]
