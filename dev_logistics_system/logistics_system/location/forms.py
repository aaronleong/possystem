from django import forms

from database.models import Location


# Django ModelForm
class LocationForm(forms.ModelForm):
    class Meta:
        model = Location
        fields = ['parcel', 'postcode', 'address']
        widgets = {
            'parcel': forms.Select(attrs={'class': 'form-control'}),
            'postcode': forms.Select(attrs={'class': 'form-control'}),
            'address': forms.TextInput(attrs={'class': 'form-control'})
        }
