from django.shortcuts import render, redirect, get_object_or_404
from database.models import Location
from .forms import LocationForm
from django.contrib.auth.decorators import login_required
from logistics_management.decorators import allowed_users


# Create your views here.
# location list
@login_required(login_url='login')
def location_list_view(request):
    query = Location.objects.all()
    template_name = 'location/table.html'
    context = {'location_list': query}
    return render(request, template_name, context)


# parcel location detail
@login_required(login_url='login')
def location_detail_view(request, my_id):
    obj = get_object_or_404(Location, id=my_id)
    template_name = 'location/detail.html'
    context = {
        'object': obj,
        'title': f'{obj.parcel} Location Detail'
    }
    return render(request, template_name, context)


# create parcel location
@login_required(login_url='login')
@allowed_users(allowed_role=['manager group','staff group'])
def location_create_view(request):
    form = LocationForm(request.POST or None)
    if form.is_valid():
        form.save()
        form = LocationForm()
    template_name = 'location/create.html'
    context = {'form': form}
    return render(request, template_name, context)


# delete parcel location
@login_required(login_url='login')
@allowed_users(allowed_role=['manager group'])
def location_delete_view(request, my_id):
    obj = get_object_or_404(Location, id=my_id)
    if request.method == 'POST':
        obj.delete()
        return redirect('/location')
    template_name = 'location/delete.html'
    context = {"object": obj}
    return render(request, template_name, context)


# update parcel location
@login_required(login_url='login')
@allowed_users(allowed_role=['manager group','staff group'])
def location_update_view(request, my_id):
    obj = get_object_or_404(Location, id=my_id)
    form = LocationForm(request.POST or None, instance=obj)
    if form.is_valid():
        form.save()
        return redirect('/location')
    template_name = 'location/update.html'
    context = {
        'form': form,
        'title': f"Update {obj.parcel} Information"
    }
    return render(request, template_name, context)
