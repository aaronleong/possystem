from django import forms

from database.models import Parcel


class ParcelForm(forms.ModelForm):
    class Meta:
        model = Parcel
        fields = ['customer', 'name', 'weight']
        widgets = {
            'customer': forms.Select(attrs={'class': 'form-control'}),
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'weight': forms.TextInput(attrs={'class': 'form-control'}),
            # no need recipent_address
            # 'recipient_address': forms.TextInput(attrs={'class': 'form-control'}),
        }
