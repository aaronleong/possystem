from django.shortcuts import render, redirect, get_object_or_404
from database.models import Parcel
from .forms import ParcelForm
from django.contrib.auth.decorators import login_required
from logistics_management.decorators import allowed_users


# Create your views here.
# parcel list
@login_required(login_url='login')
def parcel_list_view(request):
    query = Parcel.objects.all()
    template_name = 'parcel/table.html'
    context = {'parcel_list': query}
    return render(request, template_name, context)


# parcel detail
@login_required(login_url='login')
def parcel_detail_view(request, my_id):
    obj = get_object_or_404(Parcel, id=my_id)
    template_name = 'parcel/detail.html'
    context = {
        'object': obj,
        'title': f'{obj.name} Parcel Detail'
    }
    return render(request, template_name, context)


# create new parcel
@login_required(login_url='login')
@allowed_users(allowed_role=['manager group','staff group'])
def parcel_create_view(request):
    form = ParcelForm(request.POST or None)
    if form.is_valid():
        form.save()
        form = ParcelForm()
    template_name = 'parcel/create.html'
    context = {'form': form}
    return render(request, template_name, context)


# delete parcel
@login_required(login_url='login')
@allowed_users(allowed_role=['manager group'])
def parcel_delete_view(request, my_id):
    obj = get_object_or_404(Parcel, id=my_id)
    if request.method == "POST":
        obj.delete()
        return redirect("/parcel")
    template_name = 'parcel/delete.html'
    context = {"object": obj}
    return render(request, template_name, context)


# update parcel
@login_required(login_url='login')
@allowed_users(allowed_role=['manager group','staff group'])
def parcel_update_view(request, my_id):
    obj = get_object_or_404(Parcel, id=my_id)
    form = ParcelForm(request.POST or None, instance=obj)
    if form.is_valid():
        form.save()
        return redirect("/parcel")
    template_name = 'parcel/update.html'
    context = {
        'form': form,
        'title': f"Update {obj.name} Information"
    }
    return render(request, template_name, context)