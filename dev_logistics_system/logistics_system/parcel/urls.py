from django.urls import path

from . import views

app_name = 'parcel'
urlpatterns = [
    path('', views.parcel_list_view),
    path('<int:my_id>/', views.parcel_detail_view),
    path('parcel-new/', views.parcel_create_view),
    path('<int:my_id>/delete', views.parcel_delete_view),
    path('<int:my_id>/edit', views.parcel_update_view),
]
