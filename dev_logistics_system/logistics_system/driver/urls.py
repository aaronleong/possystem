from django.urls import path

from . import views

app_name = 'driver'
urlpatterns = [
    path('', views.driver_list_view),
    path('<int:my_id>/', views.driver_detail_view),
    path('driver-new/', views.driver_create_view),
    path('<int:my_id>/delete', views.driver_delete_view),
    path('<int:my_id>/edit', views.driver_update_view)
]
