from django import forms
from database.models import Driver


# Django ModelForm
class DriverForm(forms.ModelForm):
    class Meta:
        model = Driver
        fields = ['lorry', 'first_name', 'last_name', 'status']
        widgets = {
            'lorry': forms.Select(attrs={'class': 'form-control'}),
            'first_name': forms.TextInput(attrs={'class': 'form-control'}),
            'last_name': forms.TextInput(attrs={'class': 'form-control'}),
            'status': forms.Select(attrs={'class': 'form-control'}),
        }
