from django.shortcuts import render, redirect, get_object_or_404
from database.models import Driver
from .forms import DriverForm
from django.contrib.auth.decorators import login_required
from logistics_management.decorators import allowed_users


# Create your views here.
# driver list
@login_required(login_url='login')
def driver_list_view(request):
    query = Driver.objects.all()
    template_name = 'driver/table.html'
    context = {'driver_list': query}
    return render(request, template_name, context)


# driver detail
@login_required(login_url='login')
def driver_detail_view(request, my_id):
    obj = get_object_or_404(Driver, id=my_id)
    template_name = 'driver/detail.html'
    context = {
        'object': obj,
        'title': f"{obj.first_name} {obj.last_name} Detail"
    }
    return render(request, template_name, context)


# create new driver
@login_required(login_url='login')
@allowed_users(allowed_role=['manager group','staff group'])
def driver_create_view(request):
    form = DriverForm(request.POST or None)
    if form.is_valid():
        form.save()
        form = DriverForm()
    template_name = 'driver/create.html'
    context = {'form': form}
    return render(request, template_name, context)


# delete driver
@login_required(login_url='login')
@allowed_users(allowed_role=['manager group'])
def driver_delete_view(request, my_id):
    obj = get_object_or_404(Driver, id=my_id)
    if request.method == 'POST':
        obj.delete()
        return redirect('/driver')
    template_name = 'driver/delete.html'
    context = {"object": obj}
    return render(request, template_name, context)


# update driver information
@login_required(login_url='login')
@allowed_users(allowed_role=['manager group','staff group'])
def driver_update_view(request, my_id):
    obj = get_object_or_404(Driver, id=my_id)
    form = DriverForm(request.POST or None, instance=obj)
    if form.is_valid():
        print('form is valid')
        form.save()
        return redirect('/driver')
    template_name = 'driver/update.html'
    context = {
        'form': form,
        'title': f"Update {obj.first_name} {obj.last_name} Information"
    }
    return render(request, template_name, context)
