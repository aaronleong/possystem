from os.path import exists
import ast
import numpy as np
from datetime import datetime
from . import genCalendar
from .distance import genDistance


# generate timetable
def generateTimetable(Name=None):
    if Name is None:
        Name = []
    newWorker = []
    # workerName = Name.split(',')

    if len(Name) == 0:
        print('No worker is detected!')
        quit()

    for i in Name:
        worker = genCalendar.Worker(i)
        newWorker.append(worker)

    timetable = genCalendar.Schedule(team=newWorker)
    now = datetime.today()
    filename = now.strftime("%d_%m_%Y") + "_timetable.txt"

    if exists(filename):
        print("File existed!")
        file = open(filename, 'r')
        data = file.readlines()
        for i in range(data[0].count(';')):
            elements = ast.literal_eval(data[0].split(';')[i])
            # print(elements, type(elements))
    else:
        file = open(filename, "w")
        for i in range(len(timetable)):
            file.writelines(str(timetable[i]) + ';')
        file.close()
        print("Calendar generated! Kindly refer to project file.")


def distributeLocation(name_list=None, destination=None):
    if destination is None:
        destination = []
    if name_list is None:
        name_list = []
    temp = []
    result = []
    for i in range(len(name_list)):
        temp = generateDistance(destination[i])
        print(temp)
        temp.insert(0, name_list[i])
        result.append(temp)
    return result


def generateDistance(Destination, Origin='MMU MELAKA', API='AIzaSyDBPBTDlaxPNG7mfRW1bO4TwPgbqFYUWvs'):
    Origin = [Origin]
    if len(Destination) == 0:
        print("Please insert your destination!")
        quit()
    if len(Origin) > 1:
        print("You can only have ONE origin!")
        quit()
    newDistance = genDistance(API=API, Origin=Origin, Address=Destination)
    data, dist_list = newDistance.requestDistance(True)
    values, count = np.unique(data['Origin_encoded'], return_counts=True)
    best_state, best_fitness = newDistance.TSPAlgorithm(dist_list, len(values) + 1)
    index = np.where(best_state == best_state.min())
    best_state = np.roll(best_state, -index[0][0])

    print('The best state found is: ', best_state)
    print('The fitness at the best state is: ', best_fitness)

    route = []
    for i in range(len(best_state)):
        for j in range(len(data)):
            if float(best_state[i]) == float(data.loc[j, 'Origin_encoded']):
                route.append(data.loc[j, 'Origin'])
                break
            elif float(best_state[i]) == float(data.loc[j, 'Destination_encoded']):
                route.append(data.loc[j, 'Destination'])
                break
            else:
                continue

    return route
