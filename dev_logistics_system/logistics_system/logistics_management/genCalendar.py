import random

class Worker:
    def __init__(self,name):
        self.name = name
        self.workingDay = 5
        self.restDay = 2

    def assign(self,number):
        if number == 0: self.restDay -=1
        else          : self.workingDay -=1

def Schedule(team):

    totalDays = 7
    totalRestDays = []
    workerRestDay = team[0].restDay
    calendar = [[1 for x in range(totalDays)] for y in range(len(team))]

    for days in range(totalDays):
        totalRestDays.append([workerRestDay, days])

    random.shuffle(totalRestDays)
    for i in range(len(calendar)):
        if not totalRestDays[1]:
            calendar[i][totalRestDays[0][1]] = 0

        else:
            calendar[i][totalRestDays[0][1]] = 0
            calendar[i][totalRestDays[1][1]] = 0

            if totalRestDays[0][0] > 1 and totalRestDays[1][0] > 1:
                totalRestDays[0][0] -= 1
                totalRestDays[1][0] -= 1

            elif totalRestDays[0][0] == 1 and totalRestDays[1][0] == 1:
                totalRestDays.pop(1)
                totalRestDays.pop(0)

            elif totalRestDays[1][0] == 1:
                totalRestDays[0][0] -= 1
                totalRestDays.pop(1)

            elif totalRestDays[0][0] == 1:
                totalRestDays[1][0] -= 1
                totalRestDays.pop(0)

            random.shuffle(totalRestDays)

    return calendar