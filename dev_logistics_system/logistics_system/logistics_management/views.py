from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from logistics_management.decorators import allowed_users
from .LogisticsMapSystem import generateTimetable, distributeLocation
from database.models import Driver, Location
import ast, random
from datetime import datetime


# Create your views here.
@login_required(login_url='login')
def home(request):
    # timetable algorithm
    query_list = Driver.objects.all()
    name = []
    for query in query_list:
        name.append(query.first_name + " " + query.last_name)
    generateTimetable(name)

    now = datetime.today()
    filename = now.strftime("%d_%m_%Y") + "_timetable.txt"

    file = open(filename, 'r')
    data = file.readlines()
    elements = []

    for i in range(data[0].count(';')):
        value = ast.literal_eval(data[0].split(';')[i])
        for j in range(len(value)):
            if value[j] == 1:
                value[j] = "work"
            else:
                value[j] = "off"

        value.insert(0, name[i])
        elements.append(value)

    template_name = 'logistics_management/home.html'
    context = {'elements_list': elements}

    return render(request, template_name, context)


@login_required(login_url='login')
@allowed_users(allowed_role=['manager group', 'driver group'])
def sorted_location_view(request):
    # location algorithm
    # preparing driver data
    driver_query = Driver.objects.filter(status="true")
    driver_list = []
    for driver in driver_query:
        driver_list.append(driver.first_name + " " + driver.last_name)

    # prepare Location data
    postcode = []
    final_loc = []
    location_query = Location.objects.all()
    for location in location_query:
        postcode.append(location.postcode)

    # function to find unique array
    location_list = list(dict.fromkeys(postcode))

    # compare drive_list with location_list
    random_driver = []
    if len(driver_list)<len(location_list):
        count = len(location_list)-len(driver_list)
        random_driver = driver_list
        random.shuffle(random_driver)
        for i in range(count):
            driver_list.append(random_driver[i])
    print(driver_list)

    # create six empty array
    for location in location_list:
        final_loc.append([])

    # sort all address according to postcode
    for i in range(len(location_list)):
        for location in location_query:
            if location_list[i] == location.postcode:
                final_loc[i].append(location.address + ", " + location.postcode + " Malacca.")

    # call destributeLocation to process the data
    sortedLocation = distributeLocation(driver_list, final_loc)

    template_name = 'logistics_management/sortedLocation.html'
    # context = {'location_list': [['a', 'b', 'c'], ['d', 'e', 'f', 'g', 'h'], ['i', 'j']]}
    context = {'location_list': sortedLocation}
    return render(request, template_name, context)
