from django.apps import AppConfig


class LogisticsManagementConfig(AppConfig):
    name = 'logistics_management'
