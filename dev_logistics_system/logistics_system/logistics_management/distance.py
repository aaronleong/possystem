import mlrose
import pandas as pd
import numpy as np
import googlemaps


class genDistance:
    def __init__(self, API, Origin, Address):
        self.API = API
        self.Origin = Origin
        self.Address = Address
        self.Station = self.Origin + self.Address

    def TSPAlgorithm(self, dist_list, values):

        fitness_dists = mlrose.TravellingSales(distances=dist_list)
        problem_fit = mlrose.TSPOpt(length=values, fitness_fn=fitness_dists,
                                    maximize=False)
        best_state, best_fitness = mlrose.genetic_alg(problem_fit, mutation_prob=0.01, max_attempts=200)

        return best_state, best_fitness

    def getDistance(self, origin, destination, gmaps):
        # print('destination:',destination)
        data = gmaps.distance_matrix(origin, destination)
        # print('data:',data)
        try:
            distance = data['rows'][0]['elements'][0]['distance']['text']
            distance = distance.split(' ')[0]
            duration = data['rows'][0]['elements'][0]['duration']['text'].split(' ')[0]
        except:
            print("Invalid Address, please check the following address: ", destination)
        return distance, duration

    def requestDistance(self, use_duration=False):
        name = []
        data = []
        encoded = []
        if len(self.API) == 0:
            print("NO GOOGLE API KEY DETECTED!")
            quit()
        gmaps = googlemaps.Client(key=self.API)
        list = self.Station
        for i in range(len(list)):
            for j in range(i + 1, len(list)):
                distance, duration = self.getDistance(list[i], list[j], gmaps)
                name.append([list[i], list[j]])
                data.append([int(i), int(j), float(distance), float(duration)])
        name = np.asarray(name)
        data = np.asarray(data)
        data = np.concatenate((name, data), axis=1)
        for i in range(len(data)):
            if use_duration == False:
                encoded.append((int(float(data[i, 2])), int(float(data[i, 3])), float(data[i, 4])))
            elif use_duration == True:
                encoded.append((int(float(data[i, 2])), int(float(data[i, 3])), float(data[i, 5])))
        data = pd.DataFrame(data,
                            columns=["Origin", "Destination", "Origin_encoded", "Destination_encoded", "Distance(km)",
                                     "Duration(m)"])
        return data, encoded
