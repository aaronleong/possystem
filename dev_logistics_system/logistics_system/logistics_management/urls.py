from django.urls import path

from . import views

app_name = 'logistics_management'
urlpatterns = [
    path('', views.home, name='home'),
    path('sorted-location/', views.sorted_location_view)
]
