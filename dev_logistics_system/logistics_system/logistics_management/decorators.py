from django.http import HttpResponse
from django.shortcuts import redirect, render


# Introduction to Decorators
# https://www.python-course.eu/python3_decorators.php#:~:text=A%20decorator%20in%20Python%20is,a%20modified%20function%20or%20class.&text=You%20may%20also%20consult%20our%20chapter%20on%20memoization%20with%20decorators.

def allowed_users(allowed_role=[]):
    def decorator(view_fuc):
        def wrapper_func(request, *args, **kwargs):
            group = None
            if request.user.groups.exists():
                group = request.user.groups.all()[0].name
            if group in allowed_role:
                return view_fuc(request, *args, **kwargs)
            else:
                # return HttpResponse("You are not authorized to view this page")
                template_name = 'logistics_management/notAuthorized.html'
                return render(request,template_name)
        return wrapper_func
    return decorator
