from django.contrib import admin

from .models import *

# Register your models here.
admin.site.register(Customer)
admin.site.register(Parcel)
admin.site.register(Location)
admin.site.register(Lorry)
admin.site.register(Driver)

