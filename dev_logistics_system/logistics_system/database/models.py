from django.db import models

BOOLEAN_FIELDS = (
    ('true', 'TRUE'),
    ('false', 'FALSE'),
)

MELAKA_ZIP_CODE = (
    ('75000', '75000'),
    ('75150', '75150'),
    ('75260', '75260'),
    ('75400', '75400'),
    ('75460', '75460'),
    ('76450', '76450'),
    ('75450', '75450'),
)


# Create your models here.
class Customer(models.Model):
    email = models.EmailField(max_length=30, unique=True)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    phone_no = models.CharField(max_length=30)
    address = models.CharField(max_length=200)

    def __str__(self):
        return self.first_name + " " + self.last_name

    def get_absolute_url(self):
        return f'/customer/{self.pk}'

    def get_edit_url(self):
        return f'/customer/{self.pk}/edit'

    def get_delete_url(self):
        return f'/customer/{self.pk}/delete'


class Parcel(models.Model):
    customer = models.ForeignKey(Customer, blank=True, null=True, on_delete=models.SET_NULL)
    name = models.CharField(max_length=30)
    weight = models.CharField(max_length=30)
    # recipient_address = models.CharField(max_length=200)
    # status = models.
    datetime = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return f'/parcel/{self.pk}'

    def get_edit_url(self):
        return f'/parcel/{self.pk}/edit'

    def get_delete_url(self):
        return f'/parcel/{self.pk}/delete'


class Location(models.Model):
    parcel = models.ForeignKey(Parcel, blank=True, null=True, on_delete=models.SET_NULL)
    postcode = models.CharField(max_length=10, choices=MELAKA_ZIP_CODE, default='75000')
    address = models.CharField(max_length=200)

    def __str__(self):
        return self.postcode

    def get_absolute_url(self):
        return f'/location/{self.pk}'

    def get_edit_url(self):
        return f'/location/{self.pk}/edit'

    def get_delete_url(self):
        return f'/location/{self.pk}/delete'


class Lorry(models.Model):
    # parcel = models.ForeignKey(Parcel, blank=True, null=True, on_delete=models.SET_NULL)
    description = models.CharField(max_length=200)
    status = models.CharField(max_length=20, choices=BOOLEAN_FIELDS, default='false')
    datetime = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.description

    def get_absolute_url(self):
        return f'/lorry/{self.pk}'

    def get_edit_url(self):
        return f'/lorry/{self.pk}/edit'

    def get_delete_url(self):
        return f'/lorry/{self.pk}/delete'


class Driver(models.Model):
    lorry = models.ForeignKey(Lorry, blank=True, null=True, on_delete=models.SET_NULL)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    status = models.CharField(max_length=20, choices=BOOLEAN_FIELDS, default='false')
    datetime = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.first_name + " " + self.last_name

    def get_absolute_url(self):
        return f'/driver/{self.pk}'

    def get_edit_url(self):
        return f'/driver/{self.pk}/edit'

    def get_delete_url(self):
        return f'/driver/{self.pk}/delete'
