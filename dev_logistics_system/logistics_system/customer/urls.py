from django.urls import path

from . import views

app_name = 'customer'
urlpatterns = [
    path('', views.customer_list_view),
    path('<int:my_id>/', views.customer_detail_view),
    path('customer-new/', views.customer_create_view),
    path('<int:my_id>/delete', views.customer_delete_view),
    path('<int:my_id>/edit', views.customer_update_view),
]
