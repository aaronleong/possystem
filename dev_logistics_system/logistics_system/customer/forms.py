from django import forms

from database.models import Customer


# Django ModelForm
class CustomerForm(forms.ModelForm):
    class Meta:
        model = Customer
        fields = ['first_name', 'last_name', 'email', 'phone_no', 'address']
        widgets = {
            'first_name': forms.TextInput(attrs={'class': 'form-control'}),
            'last_name': forms.TextInput(attrs={'class': 'form-control'}),
            'email': forms.TextInput(attrs={'class': 'form-control', 'type': 'email'}),
            'phone_no': forms.TextInput(attrs={'class': 'form-control'}),
            'address': forms.TextInput(attrs={'class': 'form-control'}),
        }

    def clean_email(self, *args, **kwargs):
        instance = self.instance
        email = self.cleaned_data.get('email')
        query = Customer.objects.filter(email__iexact=email)
        if instance is not None:
            query = query.exclude(pk=instance.pk)
        if query.exists():
            raise forms.ValidationError("This email has already been used")
        return email
