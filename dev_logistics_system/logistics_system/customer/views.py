from django.shortcuts import render, redirect, get_object_or_404
from database.models import Customer
from .forms import CustomerForm
from django.contrib.auth.decorators import login_required
from logistics_management.decorators import allowed_users


# Create your views here.
# customer list
@login_required(login_url='login')
def customer_list_view(request):
    query = Customer.objects.all()
    template_name = 'customer/table.html'
    context = {'customer_list': query}
    return render(request, template_name, context)


# customer detail
@login_required(login_url='login')
def customer_detail_view(request, my_id):
    obj = get_object_or_404(Customer, id=my_id)
    template_name = 'customer/detail.html'
    context = {
        'object': obj,
        'title': f"{obj.first_name} {obj.last_name} Detail"
    }
    return render(request, template_name, context)


# create new customer
@login_required(login_url='login')
@allowed_users(allowed_role=['manager group','staff group'])
def customer_create_view(request):
    form = CustomerForm(request.POST or None)
    print(form)
    if form.is_valid():
        # obj = form.save()
        # obj.save()
        form.save()
        form = CustomerForm()
    template_name = 'customer/create.html'
    context = {'form': form}
    return render(request, template_name, context)


# delete customer
@login_required(login_url='login')
@allowed_users(allowed_role=['manager group'])
def customer_delete_view(request, my_id):
    obj = get_object_or_404(Customer, id=my_id)
    template_name = 'customer/delete.html'
    if request.method == "POST":
        obj.delete()
        return redirect("/customer")
    context = {"object": obj}
    return render(request, template_name, context)


# update customer
@login_required(login_url='login')
@allowed_users(allowed_role=['manager group','staff group'])
def customer_update_view(request, my_id):
    obj = get_object_or_404(Customer, id=my_id)
    form = CustomerForm(request.POST or None, instance=obj)
    if form.is_valid():
        form.save()
        return redirect("/customer")
    template_name = 'customer/update.html'
    context = {
        'form': form,
        'title': f"Update {obj.first_name} {obj.last_name} Information"
    }
    return render(request, template_name, context)
