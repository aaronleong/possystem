from django.shortcuts import render, redirect, get_object_or_404
from database.models import Lorry
from .forms import LorryForm
from django.contrib.auth.decorators import login_required
from logistics_management.decorators import allowed_users


# Create your views here.
# lorry list
@login_required(login_url='login')
def lorry_list_view(request):
    query = Lorry.objects.all()
    template_name = 'lorry/table.html'
    context = {'lorry_list': query}
    return render(request, template_name, context)


# lorry detail
@login_required(login_url='login')
def lorry_detail_view(request, my_id):
    obj = get_object_or_404(Lorry, id=my_id)
    template_name = 'lorry/detail.html'
    context = {
        'object': obj,
        'title': f'{obj.description} Lorry Detail'
    }
    return render(request, template_name, context)


# create new lorry
@login_required(login_url='login')
@allowed_users(allowed_role=['manager group','staff group'])
def lorry_create_view(request):
    form = LorryForm(request.POST or None)
    if form.is_valid():
        form.save()
        form = LorryForm()
    template_name = 'lorry/create.html'
    context = {'form': form}
    return render(request, template_name, context)


# delete lorry
@login_required(login_url='login')
@allowed_users(allowed_role=['manager group'])
def lorry_delete_view(request, my_id):
    obj = get_object_or_404(Lorry, id=my_id)
    template_name = 'lorry/delete.html'
    if request.method == 'POST':
        obj.delete()
        return redirect("/lorry")
    context = {"object": obj}
    return render(request, template_name, context)


# update lorry
@login_required(login_url='login')
@allowed_users(allowed_role=['manager group','staff group'])
def lorry_update_view(request, my_id):
    obj = get_object_or_404(Lorry, id=my_id)
    form = LorryForm(request.POST or None, instance=obj)
    print("obj: ", obj)
    if form.is_valid():
        form.save()
        return redirect('/lorry')
    template_name = 'lorry/update.html'
    context = {
        'form': form,
        'title': f"Update {obj.description} Lorry Information"
    }
    return render(request, template_name, context)