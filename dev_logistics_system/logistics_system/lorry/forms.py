from django import forms

from database.models import Lorry


class LorryForm(forms.ModelForm):
    class Meta:
        model = Lorry
        fields = ['description', 'status']
        widgets = {
            'description': forms.TextInput(attrs={'class': 'form-control'}),
            'status': forms.Select(attrs={'class': 'form-control'}),
        }

