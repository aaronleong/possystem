from django.urls import path

from . import views

app_name = 'lorry'
urlpatterns = [
    path('', views.lorry_list_view),
    path('<int:my_id>/', views.lorry_detail_view),
    path('lorry-new/', views.lorry_create_view),
    path('<int:my_id>/delete', views.lorry_delete_view),
    path('<int:my_id>/edit', views.lorry_update_view),
]
