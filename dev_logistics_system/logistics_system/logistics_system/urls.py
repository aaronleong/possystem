"""logistics_system URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.urls import include

urlpatterns = [

    path('', include('logistics_management.urls')),
    path('customer/', include('customer.urls')),
    path('parcel/', include('parcel.urls')),
    path('location/', include('location.urls')),
    path('lorry/', include('lorry.urls')),
    path('driver/', include('driver.urls')),
    # Django login and logout tutorial
    # https://learndjango.com/tutorials/django-login-and-logout-tutorial
    path('accounts/', include('django.contrib.auth.urls')),
    path('admin/', admin.site.urls),
]
